import matplotlib.pyplot as plt
import numpy as np

# Générer des données aléatoires pour l'âge et l'IMC
np.random.seed(0)
age = np.sort(np.random.randint(6, 65, size=100))
bmi = np.random.normal(loc=25, scale=5, size=100)

# Afficher le graphique
plt.plot(age, bmi, 'o', markersize=4)
plt.xlabel('Age')
plt.ylabel('IMC')
plt.title("Evolution de l'IMC en fonction de l'âge")
plt.show()