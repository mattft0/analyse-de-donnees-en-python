import numpy as np
import csv

# Générer une liste aléatoire de 10 personnes
np.random.seed(0)
names = ['Personne ' + str(i+1) for i in range(10)]
ages = np.random.randint(6, 65, size=10)
genders = np.random.choice(['Masculin', 'Féminin'], size=10)
heights = np.random.uniform(1.2, 2, size=10)
weights = np.random.uniform(20, 100, size=10)

# Calculer l'IMC de chaque personne
bmi = weights / (heights ** 2)

# Afficher le résultat
result = np.column_stack((names, ages, genders, bmi))
print("Résultat:")
for row in result:
    print(row)

# Exporter le résultat dans un fichier CSV
with open('people_bmi.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Nom', 'Age', 'Sexe', 'IMC'])
    writer.writerows(result)
