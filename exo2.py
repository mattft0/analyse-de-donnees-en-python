import random

def IMC_2():
    # Création d'une liste liste
    liste = []
    for i in range(10):
        # Créer 10 entitées dans la liste avec le nom, age, sexe, taille et poids générés aléatoirement
        liste.append({
            'nom': 'Personne {}'.format(i+1),
            'age': random.randint(6, 65),
            'sexe': random.choice(['Homme', 'Femme']),
            'taille': random.randint(120, 200),
            'poids': random.randint(20, 100)
        })
    # Calcul de l'IMC de chaque personne dans la liste 
    for personne in liste:
        personne['imc'] = personne['poids'] / (personne['taille'] / 100) ** 2
    # début des conditions pour afficher l'état physique de chaque personne dans la liste
    for personne in liste:
        print(personne['nom'],"- age :", personne['age'], "- sexe :",personne['sexe'], "- imc :", personne['imc'])
        if(personne['imc'] < 16):
            print("Vous êtes en maigreur sévère")
        elif(personne['imc'] == 16 or personne['imc'] < 17):
            print("Vous êtes en maigreur modérée")
        elif(personne['imc'] == 17 or personne['imc'] < 18,5):
            print("Vous êtes en maigreur légère")
        elif(personne['imc'] == 18,5 or personne['imc'] < 25):
            print("Vous êtes normal")
        elif(personne['imc'] == 25 or personne['imc'] < 30):
            print("Vous êtes en surpoids")
        elif(personne['imc'] == 30 or personne['imc'] < 35):
            print("Vous êtes en obésité de classe 1")
        elif(personne['imc'] == 35 or personne['imc'] < 40):
            print("Vous êtes en obésité de classe 2")
        elif(personne['imc'] >= 40):
            print("Vous êtes en obésité de classe 3")


IMC_2()
